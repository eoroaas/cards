module no.ntnu.idi.playingcards {
  requires javafx.controls;
  requires javafx.fxml;


  opens no.ntnu.idi.playingcards to javafx.fxml;
  exports no.ntnu.idi.playingcards;
}
