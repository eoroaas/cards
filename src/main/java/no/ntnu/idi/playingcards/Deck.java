package no.ntnu.idi.playingcards;

import java.util.ArrayList;
import java.util.List;

public class Deck {
  private final ArrayList<Card> cards;
  public Deck() {
    cards = new ArrayList<>();

    for (Card.Suit suit : Card.Suit.values()) {
      for (Card.Rank rank : Card.Rank.values()) {
        cards.add(new Card(suit, rank));
      }
    }
  }
  public Hand dealHand() throws IllegalArgumentException {
    if (5 > cards.size()) {
      throw new IllegalArgumentException("Not enough cards");
    }
    List<Card> cards = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      int index = (int) (Math.random() * this.cards.size());
      cards.add(this.cards.remove(index));
    }

    return new Hand(cards);
  }
  public int getCount() {
    return cards.size();
  }
}
