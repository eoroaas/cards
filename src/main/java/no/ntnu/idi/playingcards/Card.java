package no.ntnu.idi.playingcards;

public class Card {
    private final Suit suit;
    private final Rank rank;
    public char getSuitSymbol() {
        return switch (suit) {
            case SPADES -> '♠';
            case HEARTS -> '♥';
            case DIAMONDS -> '♦';
            case CLUBS -> '♣';
        };
    }
    public Card(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }
    public String getRankSymbol() {
        return switch (rank) {
            case ACE -> "A";
            case TWO -> "2";
            case THREE -> "3";
            case FOUR -> "4";
            case FIVE -> "5";
            case SIX -> "6";
            case SEVEN -> "7";
            case EIGHT -> "8";
            case NINE -> "9";
            case TEN -> "10";
            case JACK -> "J";
            case QUEEN -> "Q";
            case KING -> "K";
        };
    }
  public enum Suit {
    SPADES, HEARTS, DIAMONDS, CLUBS
  }


  public enum Rank {
    ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING
  }


  public Suit getSuit() {
    return suit;
  }


  public Rank getRank() {
    return rank;
  }


}
