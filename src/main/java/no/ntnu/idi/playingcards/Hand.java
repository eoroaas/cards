package no.ntnu.idi.playingcards;

import java.util.List;

public class Hand {
  List<Card> cards;
  public int getSumOfFaces() {
    return cards.stream().mapToInt(card -> card.getRank().ordinal() + 1).sum();
  }
  public int getCountOfHearts() {
    return (int) cards.stream().filter(card -> card.getSuit() == Card.Suit.HEARTS).count();
  }
  public boolean hasFlush() {
    return cards.stream().map(Card::getSuit).distinct().count() == 1;
  }
  public boolean hasQueenOfSpades() {
    return cards.stream()
            .anyMatch(card -> card.getSuit() == Card.Suit.SPADES && card.getRank() == Card.Rank.QUEEN);
  }
  public List<Card> cards() {
    return cards;
  }
  public Hand(List<Card> cards) {
    this.cards = cards;
  }
}
