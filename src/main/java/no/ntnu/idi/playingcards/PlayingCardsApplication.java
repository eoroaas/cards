package no.ntnu.idi.playingcards;

import java.io.IOException;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PlayingCardsApplication extends Application {
  private Deck deck;
  private Hand currentHand;

  public PlayingCardsApplication() {
    this.deck = new Deck();
  }

  @Override
  public void start(Stage stage) throws IOException {
    Button btnDealHand = new Button("Deal");
    Button btnCheckHand = new Button("Check");

    Label lblSumOfFaces = new Label("Sum:");
    Label txtSumOfFaces = new Label();
    Label lblCardsOfHearts = new Label("Hearts:");
    Label txtCardsOfHearts = new Label();
    Label lblFlush = new Label("Flush:");
    Label txtFlush = new Label();
    Label lblQueenOfSpades = new Label("Queen of spades:");
    Label txtQueenOfSpades = new Label();


    HBox hboxButtons = new HBox(10, btnDealHand, btnCheckHand);

    VBox infoBox = new VBox();

    infoBox.getChildren().addAll(lblSumOfFaces, txtSumOfFaces);
    infoBox.getChildren().addAll(lblCardsOfHearts, txtCardsOfHearts);
    infoBox.getChildren().addAll(lblFlush, txtFlush);
    infoBox.getChildren().addAll(lblQueenOfSpades, txtQueenOfSpades);

    VBox vboxMain = new VBox(10);
    vboxMain.getChildren().addAll(hboxButtons, infoBox);
    vboxMain.setPadding(new javafx.geometry.Insets(10, 10, 10, 10));

    HBox cardView = new HBox(32);


    cardView.setPrefSize(500, 500);

    vboxMain.getChildren().addFirst(cardView);

    btnDealHand.setOnAction(e -> {
      cardView.getChildren().clear();

      if (deck.getCount() < 5) {
        deck = new Deck();
      }

      Hand hand = deck.dealHand();

      for (Card card : hand.cards()) {
        cardView.getChildren().add(createCard(card));
      }

      this.currentHand = hand;
    });

    btnCheckHand.setOnAction(e -> {
      if(currentHand == null) {
        return;
      }
      txtSumOfFaces.setText(String.valueOf(currentHand.getSumOfFaces()));
      txtCardsOfHearts.setText(String.valueOf(currentHand.getCountOfHearts()));
      txtFlush.setText(currentHand.hasFlush() ? "Yes" : "No");
      txtQueenOfSpades.setText(currentHand.hasQueenOfSpades() ? "Yes" : "No");
    });

    Scene scene = new Scene(vboxMain, 500, 400);

    stage.setTitle("Frame");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch();
  }

  private StackPane createCard(Card card) {

    Label lblCard = new Label(card.getRankSymbol() + card.getSuitSymbol());

    StackPane stackPane = new StackPane();
    stackPane.setMaxWidth(50);
    if(card.getSuit() == Card.Suit.HEARTS || card.getSuit() == Card.Suit.DIAMONDS) {
      lblCard.setStyle("-fx-text-fill: red; -fx-font-size: 20px;");
    } else {
      lblCard.setStyle("-fx-text-fill: black; -fx-font-size: 20px;");
    }
    stackPane.setAlignment(Pos.CENTER);
    stackPane.getChildren().add(lblCard);

    return stackPane;
  }
}
